﻿// Copyright(c) Zachery Gyurkovitz 2017, the MIT licence, see LICENSE.md for the full licence.

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics.Contracts;
using AsyncIOSharp.Properties;

namespace AsyncIOSharp
{
    /// <summary>
    /// Provides static methods for Reading from and Writing to files.
    /// </summary>
    public static class FileAsync
    {
        public static Task AppendAllLinesAsync(string path, IEnumerable<string> contents)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }
            if (contents == null)
            {
                throw new ArgumentNullException(nameof(contents));
            }
            if (path.Length == 0)
            {
                throw new ArgumentException(Resources.Argument_EmptyPath);
            }
            Contract.EndContractBlock();

            StreamWriter sw = new StreamWriter(
                new FileStream(
                    path,
                    FileMode.Append,
                    FileAccess.Write,
                    FileShare.None,
                    BufferSize,
                    true));
            return InternalWriteAllLinesAsync(sw, contents);
        }

        public static Task AppendAllLinesAsync(string path, IEnumerable<string> contents, Encoding encoding)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }
            if (contents == null)
            {
                throw new ArgumentNullException(nameof(contents));
            }
            if (encoding == null)
            {
                throw new ArgumentNullException(nameof(encoding));
            }
            if (path.Length == 0)
            {
                throw new ArgumentException(Resources.Argument_EmptyPath);
            }
            Contract.EndContractBlock();
            StreamWriter sw = new StreamWriter(
                    new FileStream(
                        path,
                        FileMode.Append,
                        FileAccess.Write,
                        FileShare.None,
                        BufferSize,
                        true),
                    encoding);

            return InternalWriteAllLinesAsync(sw, contents);
        }

        public static Task<string> ReadAllTextAsync(string path)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }
            if (path.Length == 0)
            {
                throw new ArgumentException(Resources.Argument_EmptyPath, nameof(path));
            }
            Contract.EndContractBlock();

            return InternalReadAllTextAsync(path, Encoding.UTF8);
        }

        public static Task<string> ReadAllTextAsync(string path, Encoding encoding)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }
            if (encoding == null)
            {
                throw new ArgumentNullException(nameof(encoding));
            }
            if (path.Length == 0)
            {
                throw new ArgumentException(Resources.Argument_EmptyPath, nameof(path));
            }
            Contract.EndContractBlock();
            return InternalReadAllTextAsync(path, encoding);
        }

        public static Task WriteAllLinesAsync(string path, string[] contents)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }
            if (contents == null)
            {
                throw new ArgumentNullException(nameof(contents));
            }
            if (path.Length == 0)
            {
                throw new ArgumentException(Resources.Argument_EmptyPath);
            }
            Contract.EndContractBlock();
            StreamWriter sw = new StreamWriter(
                new FileStream(path,
                FileMode.CreateNew,
                FileAccess.Write,
                FileShare.None,
                BufferSize,
                true));
            return InternalWriteAllLinesAsync(sw, contents);
        }

        public static Task WriteAllLinesAsync(string path, string[] contents, Encoding encoding)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }
            if (contents == null)
            {
                throw new ArgumentNullException(nameof(contents));
            }
            if (encoding == null)
            {
                throw new ArgumentNullException(nameof(encoding));
            }
            if (path.Length == 0)
            {
                throw new ArgumentException(Resources.Argument_EmptyPath);
            }
            Contract.EndContractBlock();
            StreamWriter sw = new StreamWriter(
                new FileStream(path,
                FileMode.CreateNew,
                FileAccess.Write,
                FileShare.None,
                BufferSize,
                true),
                encoding);
            return InternalWriteAllLinesAsync(sw, contents);
        }

        public static Task WriteAllLinesAsync(string path, IEnumerable<string> contents)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }
            if (contents == null)
            {
                throw new ArgumentNullException(nameof(contents));
            }
            if (path.Length == 0)
            {
                throw new ArgumentException(Resources.Argument_EmptyPath);
            }
            Contract.EndContractBlock();
            StreamWriter sw = new StreamWriter(
                new FileStream(path,
                FileMode.CreateNew,
                FileAccess.Write,
                FileShare.None,
                BufferSize,
                true));
            return InternalWriteAllLinesAsync(sw, contents);
        }

        public static Task WriteAllLinesAsync(string path, IEnumerable<string> contents, Encoding encoding)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }
            if (contents == null)
            {
                throw new ArgumentNullException(nameof(contents));
            }
            if (encoding == null)
            {
                throw new ArgumentNullException(nameof(encoding));
            }
            if (path.Length == 0)
            {
                throw new ArgumentException(Resources.Argument_EmptyPath);
            }
            Contract.EndContractBlock();
            StreamWriter sw = new StreamWriter(
                new FileStream(path,
                FileMode.CreateNew,
                FileAccess.Write,
                FileShare.None,
                BufferSize,
                true),
                encoding);
            return InternalWriteAllLinesAsync(sw, contents);
        }

        public static Task WriteAllTextAsync(string path, string contents, Encoding encoding)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }
            if (encoding == null)
            {
                throw new ArgumentNullException(nameof(encoding));
            }
            if (path.Length == 0)
            {
                throw new ArgumentException(Resources.Argument_EmptyPath);
            }
            Contract.EndContractBlock();

            return InternalWriteAllTextAsync(path, contents, encoding);
        }

        public static Task WriteAllTextAsync(string path, string contents)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }
            if (contents == null)
            {
                throw new ArgumentNullException(nameof(contents));
            }
            if (path.Length == 0)
            {
                throw new ArgumentException(Resources.Argument_EmptyPath);
            }

            Contract.EndContractBlock();

            return InternalWriteAllTextAsync(path, contents, Utf8NoBom);
        }

        internal static Encoding Utf8NoBom
        {
            get
            {
                if (_UTF8NoBOM == null)
                {
                    // No need for double lock - we just want to avoid extra allocations in the
                    // common case.
                    UTF8Encoding noBOM = new UTF8Encoding(false, true);
                    System.Threading.Thread.MemoryBarrier();
                    _UTF8NoBOM = noBOM;
                }
                return _UTF8NoBOM;
            }
        }

        private const int BufferSize = 4096;

        private static volatile Encoding _UTF8NoBOM;

        private static async Task<string> InternalReadAllTextAsync(string path, Encoding encoding)
        {
            Contract.Requires(path != null);
            Contract.Requires(encoding != null);
            Contract.Requires(path.Length > 0);

            using (StreamReader reader = new StreamReader(
                new FileStream(
                    path,
                    FileMode.Open,
                    FileAccess.Read,
                    FileShare.Read,
                    BufferSize,
                    true), encoding))
            {
                return await reader.ReadToEndAsync().ConfigureAwait(false);
            }
        }

        private static async Task InternalWriteAllLinesAsync(TextWriter writer, IEnumerable<string> contents)
        {
            Contract.Requires(writer != null);
            Contract.Requires(contents != null);

            using (writer)
            {
                foreach (string line in contents)
                {
                    await writer.WriteLineAsync(line).ConfigureAwait(false);
                }
            }
        }

        private static async Task InternalWriteAllTextAsync(string path, string contents, Encoding encoding)
        {
            Contract.Requires(path != null);
            Contract.Requires(encoding != null);
            Contract.Requires(path.Length > 0);

            using (StreamWriter sw = new StreamWriter(
                new FileStream(
                    path,
                    FileMode.OpenOrCreate,
                    FileAccess.Write,
                    FileShare.None,
                    BufferSize,
                    true),
                encoding))
            {
                await sw.WriteAsync(contents).ConfigureAwait(false);
            }
        }
    }
}
